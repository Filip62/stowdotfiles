" Random Useful Functions

fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

fun! GotoWindow(id)
    call win_gotoid(a:id)
    MaximizerToggle
endfun

fun! EmptyRegisters()
    let regs=split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-"', '\zs')
    for r in regs
        call setreg(r, [])
    endfor
endfun

augroup THE_PRIMEAGEN
    autocmd!
    " Handled by a plugin
    " autocmd BufWritePre * :call TrimWhitespace()
    autocmd BufEnter,BufWinEnter,TabEnter *.rs :lua require'lsp_extensions'.inlay_hints{}
    " autocmd BufWinEnter * if &buftype == 'quickfix' | resize 20 | endif
augroup END

augroup auto_spellcheck
    autocmd!
    autocmd BufNewFile,BufRead *.md setlocal spell
augroup END

augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 40})
augroup END


if executable('rg')
    let g:rg_derive_root='true'
endif


" let loaded_matchparen = 1

" set foldmethod=expr
" set foldexpr=nvim_treesitter#foldexpr()
