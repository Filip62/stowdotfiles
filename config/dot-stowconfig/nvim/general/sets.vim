" set exrc
set clipboard=unnamedplus
set hidden
set nobackup
set nowritebackup
set noswapfile
set nowrap
set fileencoding=utf-8
set cmdheight=1
set mouse=a
set splitbelow
set splitright
set tabstop=4 softtabstop=4
set shiftwidth=4
set shiftround
set expandtab
set smartindent
set autoindent
set number
set relativenumber
set cursorline
set showtabline=2
set noshowmode
set signcolumn=yes
set updatetime=100
set timeoutlen=300
set incsearch
set nohlsearch
set ignorecase
set smartcase
set scrolloff=10
set sidescrolloff=5
set guifont=JetBrainsMono\ Nerd\ Font
set termguicolors
set wildmenu
set undodir=$HOME/.undodir
set undofile
set completeopt=menuone,noinsert,noselect
set shortmess+=c
set noerrorbells visualbell t_vb=
set nowrap
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
set list
set autoread
set colorcolumn=72,80
set efm=%f:%l:%c:\ %m,%-G%.%#errors\ generated.,%-Gmake%m,%-G[%[0-9\ ]%\\\{\\,3}%%]%m,%-GScanning\ dependencies\ of\ target%m
