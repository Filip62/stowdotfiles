let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy', 'all']
let g:completion_matching_smart_case = 1
let g:completion_enable_snippet = 'vim-vsnip'
" let g:completion_timer_cycle = 200 "default value is 80
" let g:completion_trigger_on_delete = 1
" let g:completion_trigger_character = ['.', '::']
" augroup CompletionTriggerCharacter
"     autocmd!
"     autocmd BufEnter * let g:completion_trigger_character = ['.']
"     autocmd BufEnter *.c,*.cpp let g:completion_trigger_character = ['.', '::']
" augroup end
" let g:diagnostic_virtual_text_prefix = ' '
" let g:space_before_virtual_text = 5
" let g:diagnostic_insert_delay = 1
let g:diagnostic_enable_virtual_text = 1
let g:completion_enable_auto_popup = 1
lua <<EOF
local lspconfig = require'lspconfig'
local on_attach_vim = function(client)
  require'completion'.on_attach(client)
end
lspconfig.pyls.setup{
   on_attach=on_attach_vim;
   root_dir = lspconfig.util.root_pattern('.git');
}
lspconfig.clangd.setup{
   on_attach=on_attach_vim;
}
EOF
