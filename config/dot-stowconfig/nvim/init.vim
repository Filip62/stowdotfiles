" General Settings
source $HOME/.config/nvim/general/sets.vim
source $HOME/.config/nvim/general/plugins.vim
source $HOME/.config/nvim/general/misc.vim
source $HOME/.config/nvim/general/mappings.vim
source $HOME/.config/nvim/general/lsp.vim

if exists('g:vscode')
    " VS Code extension
    source $HOME/.config/nvim/vscode/settings.vim
    source $HOME/.config/nvim/plug-config/easymotion.vim
else
    " Theme configuration
    source $HOME/.config/nvim/themes/theme.vim
    source $HOME/.config/nvim/themes/statusline.vim
    source $HOME/.config/nvim/themes/indents.vim

    " Plugin Configuration
    source $HOME/.config/nvim/plug-config/editorconfig.vim
    source $HOME/.config/nvim/plug-config/vifm.vim
    source $HOME/.config/nvim/plug-config/startscreen.vim
    source $HOME/.config/nvim/plug-config/fzf.vim
    luafile $HOME/.config/nvim/plug-config/colorizer.lua
    source $HOME/.config/nvim/plug-config/floaterm.vim
    source $HOME/.config/nvim/plug-config/gitgutter.vim
    source $HOME/.config/nvim/plug-config/sneak.vim
    source $HOME/.config/nvim/plug-config/quickscope.vim
    source $HOME/.config/nvim/plug-config/rainbow.vim
    source $HOME/.config/nvim/plug-config/indentguides.vim
    source $HOME/.config/nvim/plug-config/betterwhitespace.vim
    source $HOME/.config/nvim/plug-config/closetag.vim
    source $HOME/.config/nvim/plug-config/async.vim
    source $HOME/.config/nvim/plug-config/vimspector.vim
    source $HOME/.config/nvim/plug-config/maximizer.vim
    source $HOME/.config/nvim/plug-config/loupe.vim
    source $HOME/.config/nvim/plug-config/vim-rooter.vim
    luafile $HOME/.config/nvim/plug-config/nvim-treesitter.lua
    luafile $HOME/.config/nvim/plug-config/nvim-web-devicons.lua
"   source $HOME/.config/nvim/keys/which-key.vim
"   source $HOME/.config/nvim/plug-config/goyo.vim
"   source $HOME/.config/nvim/plug-config/xtabline.vim
"   source $HOME/.config/nvim/plug-config/polyglot.vim
"   source $HOME/.config/nvim/plug-config/far.vim
endif

" augroup fix
    " autocmd!
    " autocmd VimEnter * :silent exec "!kill -s SIGWINCH $fish_pid"
" augroup END
