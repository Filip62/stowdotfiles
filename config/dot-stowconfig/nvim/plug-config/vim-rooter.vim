let g:rooter_targets = '/,*'
let g:rooter_patterns = ['.rooter', '.git', 'Makefile']
let g:rooter_change_directory_for_non_project_files = 'current'
let g:rooter_cd_cmd = 'lcd'
let g:rooter_resolve_links = 0
