set background=dark

let g:gruvbox_contrast_dark = "hard"
let g:gruvbox_bold = 1
let g:gruvbox_itallic = 1
let g:gruvbox_italicize_strings = 0
let g:gruvbox_invert_selection= 0
" colorscheme gruvbox

let g:gruvbox_material_background = 'hard'
let g:gruvbox_material_enable_italic = 1
let g:gruvbox_material_disable_italic_comment = 0
let g:gruvbox_material_enable_bold = 1
let g:gruvbox_material_sign_column_background = 'none'
let g:gruvbox_material_diagnostic_line_highlight = 1
let g:gruvbox_material_palette = 'mix'
let g:gruvbox_material_visual = 'grey background'
let g:gruvbox_material_menu_selection_background = 'grey'
" colorscheme gruvbox-material

let g:nord_cursor_line_number_background = 1
let g:nord_uniform_status_lines = 0
let g:nord_bold_vertical_split_line = 0
let g:nord_uniform_diff_background = 1
let g:nord_bold = 1
let g:nord_italic = 1
let g:nord_italic_comments = 0
let g:nord_underline = 1
colorscheme nord
