set -x TERMINAL alacritty

function fish_prompt
    set -l last_pipestatus $pipestatus # save previous pipestatus
    set -l exit_code $pipestatus[-1] # save previous exit code
    set -l normal (set_color normal)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" (set_color $fish_color_status) (set_color --bold $fish_color_status) $last_pipestatus)

    if test -n "$prompt_status"
        echo -e \n$prompt_status
    end

    set_color normal

    switch "$fish_key_bindings"
        case fish_hybrid_key_bindings fish_vi_key_bindings
            set keymap "$fish_bind_mode"
        case '*'
            set keymap insert
    end

    # Account for changes in variable name between v2.7 and v3.0
    set -l starship_duration "$CMD_DURATION$cmd_duration"
    "/usr/local/bin/starship" prompt --status=$exit_code --keymap=$keymap --cmd-duration=$starship_duration --jobs=(count (jobs -p))

    # # Color the prompt differently when we're root
    # set -l color_cwd $fish_color_cwd
    # set -l prefix
    # set -l suffix '>'
    # if contains -- $USER root toor
    #     if set -q fish_color_cwd_root
    #         set color_cwd $fish_color_cwd_root
    #     end
    #     set suffix '#'
    # end
    #
    # # If we're running via SSH, change the host color.
    # set -l color_host $fish_color_host
    # if set -q SSH_TTY
    #     set color_host $fish_color_host_remote
    # end
    #
    # echo -n -s (set_color $fish_color_user) "$USER" $normal @ (set_color $color_host) (prompt_hostname) $normal ' ' (set_color $color_cwd) (prompt_pwd) $normal (fish_vcs_prompt) $normal $prompt_status $suffix " "

end

# disable virtualenv prompt, it breaks starship
set VIRTUAL_ENV_DISABLE_PROMPT 1

function fish_mode_prompt; end
set -x STARSHIP_SHELL "fish"

# Set up the session key that will be used to store logs
set -x STARSHIP_SESSION_KEY ("/usr/local/bin/starship" session)

# Emulates vim's cursor shape behavior
# Set the normal and visual mode cursors to a block
set fish_cursor_default block
# Set the insert mode cursor to a line
set fish_cursor_insert line
# Set the replace mode cursor to an underscore
set fish_cursor_replace_one underscore
# The following variable can be used to configure cursor shape in
# visual mode, but due to fish_cursor_default, is redundant here
set fish_cursor_visual block

# set -x PAGER "nvim +Man! - -R -c 'set nowrap nu signcolumn=no syntax=no'"
# set -x PAGER "./neovim/runtime/macros/less.sh"
set -x EDITOR "nvim"
set -x PAGER "less -iFRXM"
set -x LESS "-iFRXM"
set -x BAT_PAGER "less -iFRXM"
set -x MANPAGER "nvim +Man! -R -c 'set signcolumn=no'"

set -x FZF_DEFAULT_COMMAND "fd --type f --hidden --follow"
set -x FZF_ALT_C_COMMAND "fd --type d --hidden --follow"
set -x FZF_CTRL_T_COMMAND "fd . \$dir --hidden --follow"

set -x SUDO_ASKPASS "$HOME/.local/scripts/dmenupass"

set -x QT_STYLE_OVERRIDE kvantum
set -x XDG_CURRENT_DESKTOP GNOME

# This has to be in .profile, otherwise it runs dmenu
# set -x CM_LAUNCHER "rofi"
set -x CM_HISTLENGTH "25"

set -x CM_MAX_CLIPS "50"
# set -x CM_DIR ""
# set -x CM_OUTPUT_CLIP ""
# set -x CM_DEBUG ""
# set -x CM_DIR ""
# set -x CM_ONESHOT ""
# set -x CM_OWN_CLIPBOARD ""
# set -x CM_SELECTIONS ""
# set -x CM_IGNORE_WINDOW ""

# Found a better solution to this
# if test -e $XDG_CONFIG_HOME/dircolors/src/dir_colors -a -r $XDG_CONFIG_HOME/dircolors/src/dir_colors
#       export (echo (dircolors $XDG_CONFIG_HOME/dircolors/src/dir_colors) | sed -e 's/; export LS_COLORS//' | sed -e 's/\'//g')
# end

# [ -n "$DISPLAY" ]  && command -v xdo >/dev/null 2>&1 && xdo id > /tmp/term-wid-"$fish_pid"
# trap "rm -f /tmp/term-wid-"$fish_pid"" EXIT HUP

test -e ~/.cache/wal/fish-colors-tty.sh && source ~/.cache/wal/fish-colors-tty.sh

set -gx GPG_TTY (tty)
gpg-connect-agent updatestartuptty /bye &>/dev/null
set -e SSH_AGENT_PID
set -gx SSH_AUTH_SOCK "$XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh"
# set -gx SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)

# set -e SSH_AGENT_PID
# if test "$gnupg_SSH_AUTH_SOCK_by" -ne $fish_pid 2>/dev/null
#     set -gx SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
# end


bind jk -M insert "if commandline -P; commandline -f cancel; else; set fish_bind_mode default; commandline -f backward-char repaint-mode; end"
bind kj -M insert "if commandline -P; commandline -f cancel; else; set fish_bind_mode default; commandline -f backward-char repaint-mode; end"

bind -M insert \el echo\ -n\ \(clear\ \|\ string\ replace\ \\e\\\[3J\ \"\"\)\;\ commandline\ -f\ repaint
bind \el echo\ -n\ \(clear\ \|\ string\ replace\ \\e\\\[3J\ \"\"\)\;\ commandline\ -f\ repaint
bind -M visual \el echo\ -n\ \(clear\ \|\ string\ replace\ \\e\\\[3J\ \"\"\)\;\ commandline\ -f\ repaint

# bind --preset -M insert \el __fish_list_current_token
# bind --preset \el __fish_list_current_token
# bind --preset -M visual \el __fish_list_current_token

# bind --preset -M insert \cl echo\ -n\ \(clear\ \|\ string\ replace\ \\e\\\[3J\ \"\"\)\;\ commandline\ -f\ repaint
# bind --preset \cl echo\ -n\ \(clear\ \|\ string\ replace\ \\e\\\[3J\ \"\"\)\;\ commandline\ -f\ repaint
# bind --preset -M visual \cl echo\ -n\ \(clear\ \|\ string\ replace\ \\e\\\[3J\ \"\"\)\;\ commandline\ -f\ repaint
