# Defined via `source`
function cm-12.1-adb --wraps=/mnt/ssd/android/cm-12.1/out/host/linux-x86/bin/adb --description 'alias cm-12.1-adb /mnt/ssd/android/cm-12.1/out/host/linux-x86/bin/adb'
  /mnt/ssd/android/cm-12.1/out/host/linux-x86/bin/adb $argv; 
end
