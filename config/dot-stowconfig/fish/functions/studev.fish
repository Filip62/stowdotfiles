# Defined in - @ line 1
function studev --wraps='sudo systemctl stop udevmon' --description 'alias studev sudo systemctl stop udevmon'
  sudo systemctl stop udevmon $argv;
end
