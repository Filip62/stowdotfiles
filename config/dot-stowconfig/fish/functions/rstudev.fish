# Defined in - @ line 1
function rstudev --wraps='systemctl restart udevmon' --description 'alias rstudev systemctl restart udevmon'
  systemctl restart udevmon $argv;
end
