# Defined in - @ line 1
function sudev --wraps='sudo systemctl start udevmon' --description 'alias sudev sudo systemctl start udevmon'
  sudo systemctl start udevmon $argv;
end
