# Defined in - @ line 1
function la --wraps='exa --long --group --classify --all' --description 'alias la=exa --long --group --classify --all'
  exa --long --group --classify --all $argv;
end
