# Defined in - @ line 1
function ls --wraps='exa --long --group --classify' --description 'alias ls=exa --long --group --classify'
  exa --long --group --classify $argv;
end
