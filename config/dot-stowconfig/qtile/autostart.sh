#!/bin/fish
nitrogen --restore &
xset r rate 210 40
dunst &
unclutter -b
redshift-gtk &
picom &
flameshot &
clipmenud &
# sxhkd &
#light-locker &
